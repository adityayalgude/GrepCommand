#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "queue.h"
//sad
void qinit(queue *q) {
	q->head = NULL;
	q->tail = NULL;
}

void enq(queue *q, char str[], int flag) {
	node *tmp;
	tmp = (node *)malloc(sizeof(node));
	strcpy(tmp->d.str, str);
	tmp->d.flag = flag;
	if(q->head == NULL) {
		q->head = tmp;
		q->tail = tmp;
		tmp->next = NULL;
	}
	else {
		q->tail->next = tmp;
		q->tail = q->tail->next;
		q->tail->next = NULL;
	}
} 

data deq(queue *q) {	
	node *tmp;
	data d;
    	tmp = q->head;
    	strcpy(d.str, tmp->d.str);
    	d.flag = tmp->d.flag;
    	q->head = (q->head)->next;    
	free(tmp);
	return (d);                
}

int qempty(queue *q) {
	return q->head == NULL;
}

int qfull(queue *q){
	return 0;
}

void traverse(queue *q){
	node *tmp;
	tmp = q->head;
	while(tmp) {
		printf("%s\t%d\n", tmp->d.str, tmp->d.flag);
		tmp = tmp->next;
	}
}
