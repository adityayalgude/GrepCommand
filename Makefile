project: main.o functions.o queue.o find.o 
	cc main.o functions.o queue.o find.o -o project
main.o: main.c functions.o queue.o find.o 
	cc -c main.c
functions.o: functions.c functions.h  queue.o 
	cc -c functions.c
queue.o: queue.c queue.h  
	cc -c queue.c
find.o: find.c queue.o
	cc -c find.c
clean: 
	rm *.o
