typedef struct data {
	char str[1024];
	int flag;
}data;

typedef struct node {
	data d;
	struct node *next;
}node;

typedef struct queue{
	node *head,*tail;
}queue;
	
void qinit(queue *q);
void enq(queue *q, char str[], int flag); 
data deq(queue *q);
int qempty(queue *q);
int qfull(queue *q);
void traverse(queue *q);
