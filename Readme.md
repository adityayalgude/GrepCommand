Title of Project: Grep Command
Name: Aditya Sanjay Yalgude
MIS id: 111603075

I have done grep command. For this I have opened file and stored all data one per line in each node of queue. After this according to options given on command line argument I have processed data and put a flag for each node. And after processig data I have given output.

Following are options and their discription.
	-i : Ignore case
		Ignore case distinctions in both PATTERN and the input files.
	
	-w : Word
	`	Select only those lines containing matches that form whole words.
	
	-v : Invert-Match
		Invert the sense of matching, to select non-matching lines.
	
	-c : Count
		Display the count of lines matched according to other option.
	
	-H : With Filename
		Print the matched lines with file name.(and according other options)
	
	-h : No filename
		Suppress the filename while printing the matched words.
	
	-m NUM : Max Count==NUM
		Give output of matched lines in each file while count is equal to NUM.
	
	-b : Byte Offset
		Print the 0 based byte offset within the input file before each line of output.
	
	-q : Quiet
		Exit immediately after match is found and do not write anything in output.
	
	-r : Recursive
		Read all files under the directory and give output.
	
	-e PATTERN : 
		Use PATTERN as PATTERN for file. You can use this option multiple times.
	
	-f FILE :
		Obtain PATTERN from FILE one per line and find the matching lines.
