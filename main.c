#define _GNU_SOURCE
#include <stdio.h>
#include <stdlib.h>
#include <strings.h>
#include <string.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <dirent.h>
#include <getopt.h>
#include "functions.h"
/*This functions store data from a file in queue.*/
void storedata(int fd, queue *q) {
	data d;
	char ch;
	int i = 0;
	while(read(fd, &ch, sizeof(char))) {	
		if(ch == '\n') {
			(d.str[i]) = '\0';
			enq(q, d.str, -1);
			i = 0;
		}
		else {
			(d.str[i]) = ch;
			i++;
		}
			
	}
}

void find(queue *q, char *word, int casesen, int wordmatch, int qflag);

int main(int argc, char *argv[]) {
	int fd, vflag = 0, cflag = 0, Hflag = 0, mflag = 0, bflag = 0, qflag = 0, hflag = 0, marg;
	int rflag = 0, eflag = 0, fflag = 0, j = 0, i = 0, k, ffd, f = 0, option, casesen = 0, wordmatch = 0;
	node *tmp;
	char eargv[10][32], fargv[10][32], ch,rargv[500][500];
	data d;
	queue s;
	struct stat st_buf;
	for(k = 1; k < 400; k++) {
		rargv[k][0] = '\0';
	}
	for(k = 1; k < 10; k++) {
		eargv[k][0] = '\0';
	}
	for(k=1; k<10; k++){
		fargv[k][0] = '\0';
	}
		
	while((option = getopt(argc, argv, "iwvcHhm:bqre:f:")) != -1) {
		
		switch (option) {
			case 'i':
				casesen = 1;
				break;
				
			case 'w':
				wordmatch = 1;
				break;
				
			case 'v':
				vflag = 1;
				break;
			
			case 'c':
				cflag = 1;
				break;
			
			case 'H':
				Hflag = 1;
				break;

			case 'm':
				mflag = 1;
				marg = atoi(optarg);
				break;
				
			case 'b':
				bflag = 1;
				break;
				
			case 'q':
				qflag = 1;
				break;
				
			case 'h':
				hflag = 1;
				break;
				
			case 'r':
				rflag = 1;
				break;
			case 'e':
				eflag = 1;
				strcpy(eargv[j], optarg);
				j++;
				break;
				
			case 'f':
				fflag = 1;
				ffd = open(optarg, O_RDONLY);
				if(fd == -1) {
					printf("Usage:./project <OPTIONS> <PATTERN> <FILENAME>\n");
					printf("Option -m,-e and -f require an argument.\n");
					perror("Open failed");
					return errno;
				}
				while(read(ffd, &ch, sizeof(char))) {	
					if(ch == '\n') {
						(d.str[i]) = '\0';
						strcpy(fargv[f], d.str);
						f++;
						i = 0;
					}
					else {
						(d.str[i]) = ch;
						i++;
					}
				}
				break;			
		}
			
	}

	if(rflag == 1) {	
		DIR *dir;
		struct dirent *sd;
		dir = opendir("./");
		if(dir == NULL) {
			printf("Error! Unable to open directory.\n");
			exit(1);
		}
		while((sd = readdir(dir)) != NULL) {
			int length = strlen(sd->d_name);
			if(strncmp(sd->d_name + length - 4, ".txt", 4) == 0 || strncmp(sd->d_name + length - 4, ".cpp", 4) == 0 ||
			strncmp(sd->d_name + length - 2, ".c", 2) == 0 || strncmp(sd->d_name + length - 2, ".h", 2) == 0 ||
			strncmp(sd->d_name + length - 5, ".java", 5) == 0 ||	strncmp(sd->d_name + length - 3, ".py", 3) == 0) {
				fd = open(sd->d_name, O_RDONLY);
				qinit(&s);
				storedata(fd, &s);
				if(eflag == 1 || fflag == 1) {	
					if(eflag == 1) {
						j=0;
						while(eargv[j][0] != '\0') {
							find(&s, eargv[j], casesen, wordmatch, qflag);
							j++;
						}
					}
		
					if(fflag == 1) {
						f = 0;
						while(fargv[f][0] != '\0') {
							find(&s, fargv[f], casesen, wordmatch, qflag);
							f++;
						}
					}
				}
				else				
					find(&s, argv[optind], casesen, wordmatch, qflag);
	
				if (bflag == 1) {	
					if(mflag == 1) {		
						if(cflag == 1) {
							if(vflag == 1 && hflag == 1)
								printcountmb(&s, -1, marg);
							else if(vflag == 1 && hflag == 0)
								printfilecountmb(&s, -1, sd->d_name, marg);
							else if(vflag == 0 && hflag == 1)
								printcountmb(&s, 1, marg);
							else
								printfilecountmb(&s, 1, sd->d_name, marg);
						}
						else
						{
							if(vflag == 1 && hflag == 1)
								printdatamb(&s, -1, marg);
							else if(vflag == 1 && hflag == 0)
								printfiledatamb(&s, -1, sd->d_name, marg);
							else if(vflag == 0 && hflag == 1)
								printdatamb(&s, 1, marg);
							else
								printfiledatamb(&s, 1, sd->d_name, marg);
		
						}
					}
					else {
						if(cflag == 1) {
							if(vflag == 1 && hflag == 1)
								printcountb(&s, -1);
							else if(vflag == 1 && hflag == 0)
								printfilecountb(&s, -1, sd->d_name);
							else if(vflag == 0 && hflag == 1)
								printcountb(&s,1);
							else
								printfilecountb(&s, 1, sd->d_name);
		
						}
						else {
							if(vflag == 1 && hflag == 1)
								printdatab(&s, -1);
							else if(vflag == 1 && hflag == 0)
								printfiledatab(&s, -1, sd->d_name);
							else if(vflag == 0 && hflag == 1)
								printdatab(&s, 1);
							else
								printfiledatab(&s, 1, sd->d_name);
		
						}
					}
				}	
				else {
					if(mflag == 1) {		
						if(cflag == 1) {
							if(vflag == 1 && hflag == 1)
								printcountm(&s, -1, marg);
							else if(vflag == 1 && hflag == 0)
								printfilecountm(&s, -1, sd->d_name, marg);
							else if(vflag == 0 && hflag == 1)
								printcountm(&s, 1, marg);
							else
								printfilecountm(&s, 1, sd->d_name, marg);
						}
						else {
							if(vflag == 1 && hflag == 1)
								printdatam(&s, -1, marg);
							else if(vflag == 1 && hflag == 0)
								printfiledatam(&s, -1, sd->d_name, marg);
							else if(vflag == 0 && hflag == 1)
								printdatam(&s, 1, marg);
							else
								printfiledatam(&s, 1, sd->d_name, marg);
						}
					}
					else {
						if(cflag == 1) {
							if(vflag == 1 && hflag == 1)
								printcount(&s, -1);
							else if(vflag == 1 && hflag == 0)
								printfilecount(&s, -1, sd->d_name);
							else if(vflag == 0 && hflag == 1)
								printcount(&s, 1);
							else
								printfilecount(&s, 1, sd->d_name);
						}
						else {
							if(vflag == 1 && hflag == 1)
								printdata(&s, -1);
							else if(vflag == 1 && hflag == 0)
								printfiledata(&s, -1, sd->d_name);
							else if(vflag == 0 && hflag == 1)
								printdata(&s, 1);
							else
								printfiledata(&s, 1, sd->d_name);
						}
					}	
				}			
			}
			else continue;
		}
	closedir(dir);
	}

	
	else {		
		int i;
		if(eflag == 1 || fflag == 1)
			i = 0;
		else
			i = 1;
		while(argv[optind+i]) {
			fd=open(argv[optind+i],O_RDONLY);
			
			if(fd == -1){
				printf("Usage:./project <OPTIONS> <PATTERN> <FILENAME>\n");
				printf("Option -m, -e and -f require an argument.\n");
				perror("Open failed");
				return errno;
			}
			qinit(&s);
			storedata(fd,&s);
			
			if(eflag == 1) {
				j = 0;
				while(eargv[j][0] != '\0') {
					find(&s, eargv[j], casesen, wordmatch, qflag);
					j++;
				}
			}
	
			if(fflag == 1) {
				f = 0;
				while(fargv[f][0] != '\0') {
					find(&s, fargv[f], casesen, wordmatch, qflag);
					f++;
				}
			}
			else {
				find(&s,argv[optind],casesen,wordmatch,qflag);
			}
	
			if (bflag == 1) {	
				if(mflag == 1) {		
					if(cflag == 1) {
						if(vflag == 1 && Hflag == 1)
							printfilecountmb(&s, -1, argv[optind+i], marg);
						else if(vflag == 1 && Hflag == 0)
							printcountmb(&s, -1, marg);
						else if(vflag == 0 && Hflag == 1)
							printfilecountmb(&s, 1, argv[optind+i], marg);
						else
							printcountmb(&s, 1, marg);
					}
					else {
						if(vflag == 1 && Hflag == 1)
							printfiledatamb(&s, -1, argv[optind+i], marg);
						else if(vflag == 1 && Hflag == 0)
							printdatamb(&s, -1, marg);
						else if(vflag == 0 && Hflag == 1)
							printfiledatamb(&s, 1, argv[optind+i], marg);
						else
							printdatamb(&s, 1, marg);
					}
				}
				else {
					if(cflag == 1) {
						if(vflag == 1 && Hflag == 1)
							printfilecountb(&s, -1, argv[optind+i]);
						else if(vflag == 1 && Hflag == 0)
							printcountb(&s, -1);
						else if(vflag == 0 && Hflag == 1)
							printfilecountb(&s, 1, argv[optind+i]);
						else
							printcountb(&s, 1);
					}
					else {
						if(vflag == 1 && Hflag == 1)
							printfiledatab(&s, -1, argv[optind+i]);
						else if(vflag == 1 && Hflag == 0)
							printdatab(&s, -1);
						else if(vflag == 0 && Hflag == 1)
							printfiledatab(&s, 1, argv[optind+i]);
						else
							printdatab(&s, 1);
					}
				}
			}	
			else {
				if(mflag == 1) {		
					if(cflag == 1) {
						if(vflag == 1 && Hflag == 1)
							printfilecountm(&s, -1, argv[optind+i], marg);
						else if(vflag == 1 && Hflag == 0)
							printcountm(&s, -1, marg);
						else if(vflag == 0 && Hflag == 1)
							printfilecountm(&s, 1, argv[optind+i], marg);
						else
							printcountm(&s, 1, marg);
					}
					else {
						if(vflag == 1 && Hflag == 1)
							printfiledatam(&s, -1, argv[optind+i], marg);
						else if(vflag == 1 && Hflag == 0)
							printdatam(&s, -1, marg);
						else if(vflag == 0 && Hflag == 1)
							printfiledatam(&s, 1, argv[optind+i], marg);
						else
							printdatam(&s, 1, marg);
					}
				}
				else {
					if(cflag == 1) {
						if(vflag == 1 && Hflag == 1)
							printfilecount(&s, -1, argv[optind+i]);
						else if(vflag == 1 && Hflag == 0)
							printcount(&s, -1);
						else if(vflag == 0 && Hflag == 1)
							printfilecount(&s, 1, argv[optind+i]);
						else
							printcount(&s, 1);
					}
					else {
						if(vflag == 1 && Hflag == 1)
							printfiledata(&s, -1, argv[optind+i]);
						else if(vflag == 1 && Hflag == 0)
							printdata(&s, -1);
						else if(vflag == 0 && Hflag == 1)
							printfiledata(&s, 1, argv[optind+i]);
						else
							printdata(&s, 1);
					}
				}	
			}
			i++;
			close(fd);
		}
	}		
	return 0;
}
