#define _GNU_SOURCE
#include <stdio.h>
#include <stdlib.h>
#include <strings.h>
#include <string.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include "queue.h"
/* This function will see if a word matches or not 
 * with the data stored in the queue node by node and if matches 
 * then changes flag to 1.
 */ 
void find(queue *q, char *word, int casesen, int wordmatch, int qflag) {
	node *tmp;
	tmp=q->head;
	int flag;
	while(tmp) {
		if(casesen == 0 && wordmatch == 0) {	
			char *token;
			token = strstr(tmp->d.str, word);
			if(token) {
				if(qflag == 1)
					exit(1);
				tmp->d.flag = 1;
			}
		}
		if(casesen == 1 && wordmatch == 0) {	
			char *token;
			token = strcasestr(tmp->d.str, word);
			if(token) {
				if(qflag == 1)
					exit(1);
				tmp->d.flag = 1;
			}
		}
		if(casesen == 0 && wordmatch == 1) {
			char strline[1024] = " ",str[1024] = " ";
			strcat(strline, tmp->d.str);
			strcat(str, word);
			int i = 0;
			while(strline[i] != '\0') {
				i++;
			}
			strline[i] = ' ';
			strline[i+1] = '\0';
			i = 0;
			while(str[i] != '\0') {
				i++;
			}
			str[i] = ' ';
			str[i+1] = '\0';
			char *token;
			token = strstr(strline, str);
			if(token) {
				if(qflag == 1)
					exit(1);
				tmp->d.flag = 1;
			}
		}
		if(casesen == 1 && wordmatch == 1) {
			char strline[1024] = " ",str[1024] = " ";
			strcat(strline, tmp->d.str);
			strcat(str, word);
			int i = 0;
			while(strline[i] != '\0') {
				i++;
			}
			strline[i] = ' ';
			strline[i+1] = '\0';
			i = 0;
			while(str[i] != '\0') {
				i++;
			}
			str[i] = ' ';
			str[i+1] = '\0';
			char *token;
			token = strcasestr(strline, str);
			if(token) {
				if(qflag == 1)
					exit(1);
				tmp->d.flag = 1;
			}
		}	
		tmp = tmp->next;	
	}
}
