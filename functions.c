#include <stdio.h>
#include <stdlib.h>
#include <strings.h>
#include <string.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include "functions.h"
#define RED   "\x1B[31m"
#define RESET "\x1B[0m"
#define BLUE    "\x1b[34m"
#define GREEN   "\x1b[32m"
#define YELLOW  "\x1b[33m"
/*This functions prints data of matching word*/
void printdata(queue *q, int flag) {
	node *tmp;
	tmp = q->head;
	while(tmp) {
		data b = tmp->d;
		if(b.flag == flag) {
			if(flag == -1)
				printf("%s\n",b.str);
			if(flag == 1)
				printf(RED"%s\n"RESET,b.str);
		}
		tmp = tmp->next;
	}
}

/*This functions prints count of matching word*/
void printcount(queue *q, int flag) {
	node *tmp;
	int count = 0;
	tmp = q->head;
	while(tmp) {
		data b = tmp->d;
		if(b.flag == flag)
			count++;
		tmp=tmp->next;
	}
	printf("%d\n",count);
}

/*This functions prints data of matching word with the file name at first*/
void printfiledata(queue *q, int flag, char *filename) {
	node *tmp;
	tmp = q->head;
	while(tmp) {
		data b = tmp->d;
		if(b.flag == flag) {	
			if(flag == -1) {
				printf(BLUE"%s"RESET, filename);
				printf(GREEN":"RESET);
				printf("%s\n", b.str);
			}
			if(flag == 1) {
				printf(BLUE"%s"RESET, filename);
				printf(GREEN":"RESET);
				printf(RED"%s\n"RESET, b.str);
			}			
		}
		tmp = tmp->next;
	}
}

/*This functions prints count of matching word with the file name at first*/
void printfilecount(queue *q, int flag, char *filename) {
	node *tmp;
	int count = 0;
	tmp = q->head;
	while(tmp) {
		data b = tmp->d;
		if(b.flag == flag)
			count++;
		tmp = tmp->next;
	}
	printf(BLUE"%s"RESET, filename);
	printf(GREEN":"RESET);
	printf("%d\n", count);
			
}

/*This functions prints data of matching word with the MAX argument*/
void printdatam(queue *q, int flag, int marg){
	node *tmp;
	int i = 0;
	tmp = q->head;
	while((i < marg) && (tmp != NULL)) {
		data b = tmp->d;
		if(b.flag == flag) {	
			if(flag == -1)
				printf("%s\n", b.str);
			if(flag == 1)
				printf(RED"%s\n"RESET,b.str);
			i++;
		}
		tmp = tmp->next;
	}
}

/*This functions prints count of matching word with the MAX argument*/
void printcountm(queue *q, int flag, int marg) {
	node *tmp;
	int count = 0;
	tmp = q->head;
	while((count < marg) && (tmp != NULL)) {
		data b = tmp->d;
		if(b.flag == flag)
			count++;
		tmp = tmp->next;
	}
	printf("%d\n", count);
}
			
/*This functions prints data of matching word with the MAX argument
and with the file name.*/
void printfiledatam(queue *q, int flag, char *filename, int marg) {
	node *tmp;
	int i = 0;
	tmp = q->head;
	while((i < marg) && (tmp != NULL)) {
		data b = tmp->d;
		if(b.flag == flag) {
			if(flag == -1) {
				printf(BLUE"%s"RESET, filename);
				printf(GREEN":"RESET);
				printf("%s\n", b.str);
			}
			if(flag == 1) {
				printf(BLUE"%s"RESET, filename);
				printf(GREEN":"RESET);
				printf(RED"%s\n"RESET, b.str);
			}	
			i++;
		}
		tmp = tmp->next;
	}
}

/*This functions prints count of matching word with the MAX argument
and with the file name.*/
void printfilecountm(queue *q, int flag, char *filename, int marg){
	node *tmp;
	int count = 0;
	tmp = q->head;
	while((count < marg) && (tmp != NULL)) {
		data b = tmp->d;
		if(b.flag == flag)
			count++;
		tmp = tmp->next;
	}
	printf(BLUE"%s"RESET, filename);
	printf(GREEN":"RESET);
	printf("%d\n", count);
}
			
/*This functions prints data of matching word 
and with the byte position.*/
void printdatab(queue *q, int flag){
	node *tmp;
	tmp = q->head;
	int byte = 0;
	while(tmp) {
		data b = tmp->d;
		if(b.flag == flag) {	
			if(flag == -1) {			
				printf(YELLOW"%d"RESET, byte);
				printf(GREEN":"RESET);
				printf("%s\n", b.str);
			}
			if(flag == 1) {			
				printf(YELLOW"%d"RESET, byte);
				printf(GREEN":"RESET);
				printf(RED"%s\n"RESET, b.str);
			}
		}
		byte = byte + strlen(b.str) + 1;
		tmp = tmp->next;
	}
}
void printcountb(queue *q, int flag) {
	node *tmp;
	int count = 0;
	tmp = q->head;
	while(tmp) {
		data b = tmp->d;
		if(b.flag == flag)
			count++;
		tmp = tmp->next;
	}
	printf("%d\n", count);
}
			
/*This functions prints data of matching word 
and with the file name and byte position.*/
void printfiledatab(queue *q, int flag, char *filename) {
	node *tmp;
	int byte = 0;
	tmp = q->head;
	while(tmp) {
		data b = tmp->d;
		if(b.flag == flag) {
			if(flag == -1) {
				printf(BLUE"%s"RESET, filename);
				printf(GREEN":"RESET);
				printf(YELLOW"%d"RESET, byte);
				printf(GREEN":"RESET);
				printf("%s\n", b.str);
			}
			if(flag == 1) {
				printf(BLUE"%s"RESET, filename);
				printf(GREEN":"RESET);
				printf(YELLOW"%d"RESET, byte);
				printf(GREEN":"RESET);
				printf(RED"%s\n"RESET, b.str);
			}
		}
		byte = byte + strlen(b.str) + 1;
		tmp = tmp->next;
	}
}

void printfilecountb(queue *q, int flag, char *filename) {
	node *tmp;
	int count = 0;
	tmp = q->head;
	while(tmp) {
		data b = tmp->d;
		if(b.flag == flag)
			count++;
		tmp = tmp->next;
	}
	printf(BLUE"%s"RESET, filename);
	printf(GREEN":"RESET);
	printf("%d\n", count);
}

/*This functions prints data of matching word 
and with the MAX argument and byte position.*/
void printdatamb(queue *q, int flag, int marg) {
	node *tmp;
	int i = 0;
	int byte = 0;
	tmp = q->head;
	while((i < marg) && (tmp != NULL)) {
		data b = tmp->d;
		if(b.flag == flag) {
			if(flag == -1) {
				printf(YELLOW"%d"RESET, byte);
				printf(GREEN":"RESET);
				printf("%s\n", b.str);
			}				
			if(flag == 1) {
				printf(YELLOW"%d"RESET, byte);
				printf(GREEN":"RESET);
				printf(RED"%s\n"RESET, b.str);
			}
			i++;
		}
	byte = byte + strlen(b.str) + 1;
	tmp = tmp->next;
	}
}

/*This functions prints count of matching word 
and with the MAX argument and byte position.*/
void printcountmb(queue *q, int flag, int marg) {
	node *tmp;
	int count = 0;
	tmp = q->head;
	while((count < marg) && (tmp != NULL)) {
		data b = tmp->d;
		if(b.flag == flag)
			count++;
		tmp = tmp->next;
	}
	printf("%d\n", count);
}
			
/*This functions prints data of matching word 
and with the file name and MAX argument and byte position.*/
void printfiledatamb(queue *q, int flag, char *filename, int marg) {
	node *tmp;
	int i = 0;
	int byte = 0; 
	tmp = q->head;
	while((i < marg) && (tmp != NULL)) {
		data b = tmp->d;
		if(b.flag == flag) {
			if(flag == -1) {
				printf(BLUE"%s"RESET, filename);
				printf(GREEN":"RESET);
				printf(YELLOW"%d"RESET, byte);
				printf(GREEN":"RESET);
				printf("%s\n", b.str);
			}
			if(flag == 1) {
				printf(BLUE"%s"RESET, filename);
				printf(GREEN":"RESET);
				printf(YELLOW"%d"RESET, byte);
				printf(GREEN":"RESET);
				printf(RED"%s\n"RESET, b.str);
			}
			i++;
		}
		byte = byte + strlen(b.str) + 1;
		tmp = tmp->next;
	}
}

/*This functions prints count of matching word 
and with the file name and MAX argument and byte position.*/
void printfilecountmb(queue *q, int flag, char *filename, int marg) {
	node *tmp;
	int count = 0;
	tmp = q->head;
	while((count < marg) && (tmp != NULL)) {
		data b = tmp->d;
		if(b.flag == flag)
			count++;
		tmp = tmp->next;
	}
	printf(BLUE"%s"RESET, filename);
	printf(GREEN":"RESET);
	printf("%d\n", count);
}

